import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [cpw, setCpw] = useState("")

    const [isDisabled, setIsDisabled] = useState(true)

    const [fn, setFn] = useState("");
    const [ln, setLn] = useState("");
    const [mobile, setMobile] = useState("");
    

    /*mini activity
        
        create a state for the button
            if email, pw, cpw is empty && pw does not match cpw, keep button disabled
            if states passed the requirement, activiate the button to be able to submit the form

            screenshot the state for the button & the useEffect codes
            answer by 6:20
    */

    useEffect(() => {
        if (
            email !== "" &&
            fn !== "" &&
            ln !== "" &&
            mobile !== "" &&
            password !== "" &&
            cpw !== "" &&
            password === cpw
        ) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password, cpw, fn, ln, mobile]);



    function Register (e) {
        e.preventDefault()

        
        fetch("http://localhost:4000/project/users/register",{
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        }).then(response => response.json()).then(result =>{
    
            if (result === false) {
        
                Swal.fire({
                    title: "Email already exists",
                    icon: "error",
                    text: "Use a different email"
                })
            } else {
                fetch("http://localhost:4000/project/users/register", {
                    method: "POST",
                    headers: {
                        "Content-Type" : "application/json"
                    },
                    body: JSON.stringify({
                        firstName: fn,
                        lastName: ln,
                        mobileNo: mobile,
                        email: email,
                        password: password
                    })
                }).then(response => response.json()).then(result => {
                    
                    if (result === true ) {
                        Swal.fire({
                    title: "Registration Successful!",
                    icon: "success",
                    text: "Please log in"

                })  

                    } else {
                        Swal.fire({
                    title: "Error",
                    icon: "error",
                    text: "Please try again"
                })
                    }
                })
            }
        })}



     return (
        <Container>
            <Form className="border p-3 mb-3" onSubmit={(e) => Register(e)}>
                {/*first name*/}
                <Form.Group className="mb-3" controlId="fn">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter first name"
                        value={fn}
                        onChange={(e) => setFn(e.target.value)}
                    />
                </Form.Group>
                {/*last name*/}
                <Form.Group className="mb-3" controlId="ln">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter last name"
                        value={ln}
                        onChange={(e) => setLn(e.target.value)}
                    />
                </Form.Group>
                {/*number*/}
                <Form.Group className="mb-3" controlId="mobile">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control
                        type="number"
                        placeholder="Enter mobile number"
                        value={mobile}
                        onChange={(e) => setMobile(e.target.value)}
                    />
                </Form.Group>
                {/*email*/}
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>
                {/*password*/}
                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>
                {/*confirm password*/}
                <Form.Group className="mb-3" controlId="cpw">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Verify Password"
                        value={cpw}
                        onChange={(e) => setCpw(e.target.value)}
                    />
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>
                    Submit
                </Button>
            </Form>
        </Container>
    );
}
